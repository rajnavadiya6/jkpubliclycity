<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imageUrl = 'assets/img/favicon.png';
        $favicon = 'assets/img/favicon.png';

        Setting::create(['key' => 'application_name', 'value' => 'Navkar Digital']);
        Setting::create(['key' => 'company_url', 'value' => 'navakardigital.com']);
        Setting::create(['key' => 'logo', 'value' => $imageUrl]);
        Setting::create(['key' => 'favicon', 'value' => $favicon]);
        Setting::create(['key' => 'company_description', 'value' => 'Leading Laravel Development Company of India']);
        Setting::create([
            'key'   => 'address',
            'value' => 'Umiya Business Bay-Tower-1, 7th Floor, Cessna Business Park,
Kadubeesanahalli, Bengaluru- 560103.']);
        Setting::create(['key' => 'phone', 'value' => '+91 70163 33243']);
        Setting::create(['key' => 'email', 'value' => 'support@navkar.com']);
        Setting::create(['key' => 'facebook_url', 'value' => 'https://www.facebook.com']);
        Setting::create(['key' => 'twitter_url', 'value' => 'https://twitter.com']);
        Setting::create(['key' => 'instagram_url', 'value' => 'https://instagram.com']);
        Setting::create([
            'key'   => 'youtube_url',
            'value' => 'https://www.youtube.com',
        ]);
        Setting::create([
            'key'   => 'pinterest_url',
            'value' => 'https://www.pinterest.com',
        ]);
    }
}
