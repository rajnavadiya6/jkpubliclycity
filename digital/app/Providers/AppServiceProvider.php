<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);
        If (env('APP_ENV','local') !== 'local') {
            $this->app['request']->server->set('HTTPS', true);
        }

        $this->app->bind('path.public', function() {
            return base_path().'/../public_html';
        });
    }
}
