<?php

namespace App\Repositories;

use App\Models\FrontSetting;
use App\Models\HomeImageSlider;
use App\Models\HomePageSlider;
use Illuminate\Support\Arr;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig;

/**
 * Class SettingRepository
 */
class FrontSettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value',
    ];

    /**
     * @inheritDoc
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @inheritDoc
     */
    public function model()
    {
        return FrontSetting::class;
    }

    /**
     * @param  $input
     *
     *
     * @return bool
     */
    public function homeUpdateSetting($request,$input)
    {

//        $oldImageIdsArr = $request->get('name');
//        /** @var HomePageSlider $homePageSlider */
//        $homePageSlider = HomePageSlider::with('media')->first();
//        if ($oldImageIdsArr) {
//            foreach ($homePageSlider->media as $media) {
//                if (!in_array($media->id, $oldImageIdsArr)) {
//                    $media->delete();
//                }
//            }
//        } else {
//            $homePageSlider->media()->delete();
//        }
//        $newFiles = $request->file('name');
//        if ($newFiles) {
//            foreach ($newFiles as $image) {
//                $homePageSlider->addMedia($image)->toMediaCollection(HomePageSlider::IMAGE_PATH);
//            }
//        }


        if (! empty($input['hero_image'])) {
            foreach ($input['hero_image'] as $index => $image) {
                $imageName = time().'_'.$image->getClientOriginalName();
                $image->move(public_path(HomePageSlider::IMG_PATH), $imageName);
                $homeImage = HomePageSlider::create([
                    'name' => HomePageSlider::IMG_PATH.'/'.$imageName,
                ]);
            }
        }


        $inputArr = Arr::except($input, ['_token']);
        foreach ($inputArr as $key => $value) {
            /** @var FrontSetting $frontSetting */
            $frontSetting = FrontSetting::where('page', 'home')->where('key', $key)->first();
            if (! $frontSetting) {
                continue;
            }

            $frontSetting->update(['value' => $value]);
        }

        return true;
    }

    /**
     * @param  array  $input
     *
     * @throws DiskDoesNotExist
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     *
     * @return bool
     */
    public function contactUsUpdateSetting($input)
    {
        $inputArr = Arr::except($input, ['_token']);
        foreach ($inputArr as $key => $value) {
            /** @var FrontSetting $frontSetting */
            $frontSetting = FrontSetting::where('page', 'contact_us')->where('key', $key)->first();
            if (! $frontSetting) {
                continue;
            }

            $frontSetting->update(['value' => $value]);
        }

        return true;
    }
}
