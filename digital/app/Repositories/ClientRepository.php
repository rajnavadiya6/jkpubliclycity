<?php

namespace App\Repositories;

use App\Models\Client;
use App\Models\ProvideSerivce;

/**
 * Class ClientRepository
 * @package App\Repositories
 * @version January 15, 2021, 9:48 am UTC
 */

class ClientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'logo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Client::class;
    }
}
