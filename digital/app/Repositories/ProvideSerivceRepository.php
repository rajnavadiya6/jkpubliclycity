<?php

namespace App\Repositories;

use App\Models\ProvideSerivce;

/**
 * Class ProvideSerivceRepository
 * @package App\Repositories
 * @version January 15, 2021, 9:48 am UTC
 */

class ProvideSerivceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProvideSerivce::class;
    }
}
