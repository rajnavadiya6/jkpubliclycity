<?php

namespace App\MediaLibrary;

use App\Models\Client;
use App\Models\ProvideSerivce;
use App\Models\Setting;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator;

/**
 * Class CustomPathGenerator
 */
class CustomPathGenerator implements PathGenerator
{
    /**
     * @param  Media  $media
     *
     * @return string
     */
    public function getPath(Media $media): string
    {
        $path = '{PARENT_DIR}'.DIRECTORY_SEPARATOR.$media->id.DIRECTORY_SEPARATOR;

        switch ($media->collection_name) {
            case Setting::IMG_PATH:
                return str_replace('{PARENT_DIR}', Setting::IMG_PATH, $path);
            case Client::IMG_PATH:
                return str_replace('{PARENT_DIR}', Client::IMG_PATH, $path);
            case ProvideSerivce::IMG_PATH:
                return str_replace('{PARENT_DIR}', ProvideSerivce::IMG_PATH, $path);
//            case Vendor::IMAGE_PATH:
//                return str_replace('{PARENT_DIR}', Vendor::IMAGE_PATH, $path);
            case 'default' :
                return '';
        }
    }

    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media).'thumbnails/';
    }

    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media).'rs-images/';
    }
}
