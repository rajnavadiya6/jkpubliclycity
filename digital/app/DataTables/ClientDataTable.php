<?php

namespace App\DataTables;

use App\Models\Client;

class ClientDataTable
{
    /**
     * @return Client
     */
    public function get()
    {
        /** @var Client $query */
        $query = Client::query()->orderByDesc('updated_at')->select('client.*');

        return $query;
    }
}
