<?php

namespace App\DataTables;

use App\Models\ProvideSerivce;

class ProvideServiceDataTable
{
    /**
     * @return ProvideSerivce
     */
    public function get()
    {
        /** @var ProvideSerivce $query */
        $query = ProvideSerivce::query()->orderByDesc('updated_at')->select('provide_services.*');

        return $query;
    }
}
