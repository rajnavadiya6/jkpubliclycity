<?php

namespace App\Http\Controllers;

use App\DataTables\ClientDataTable;
use App\Models\Client;
use App\Queries\BrandDatatable;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ClientController extends AppBaseController
{
    /** @var  ClientRepository  $clientRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the Color.
     *
     * @param Request $request
     *
     * @return View|Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of((new ClientDataTable())->get())->make(true);
        }

        return view('clients.index');
    }

    /**
     * Store a newly created Color in storage.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $client = Client::create($input);

        if (isset($input['image']) && ! empty($input['image'])) {
            $client->addMedia($input['image'])->toMediaCollection(Client::IMG_PATH);
        }

        return $this->sendSuccess('Client saved successfully.');
    }

    /**
     * Display the specified Color.
     *
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function show(Client $client)
    {
        return $this->sendResponse($client, 'Client Retrieved Successfully.');
    }

    /**
     * Show the form for editing the specified Color.
     *
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function edit(Client $client)
    {
        return $this->sendResponse($client, 'Client Retrieved Successfully.');
    }

    /**
     * Update the specified Color in storage.
     *
     * @param Request $request
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function update(Request $request,Client $client)
    {
        $input = $request->all();
        $client = $this->clientRepository->update($input, $client->id);

        if (isset($input['image']) && ! empty($input['image'])) {
            $client->clearMediaCollection(Client::IMG_PATH);
            $client->addMedia($input['image'])->toMediaCollection(Client::IMG_PATH);
        }

        return $this->sendSuccess('Client updated successfully.');
    }

    /**
     * Remove the specified Color from storage.
     *
     * @param Client $client
     *
     * @throws Exception
     *
     * @return JsonResponse
     */
    public function destroy(Client $client)
    {
        $client->clearMediaCollection(Client::IMG_PATH);
        $client->delete();

        return $this->sendSuccess('Client deleted successfully.');
    }
}
