<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\FrontSetting;
use App\Models\Gallery;
use App\Models\HomePageSlider;
use App\Models\ProvideSerivce;
use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends AppBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homeImageSliders = HomePageSlider::all();
        $clients = Client::all();
        $frontSettings = FrontSetting::where('page','home')->pluck('value','key')->toArray();
        $provideServices = ProvideSerivce::all()->sortByDesc('updated_at');

        return view('theme.home',compact('homeImageSliders','clients','frontSettings','provideServices'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ourService()
    {
        $provideServices =  ProvideSerivce::all();

        return view('theme.our_services',compact('provideServices'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutUs()
    {
        return view('theme.about');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUs()
    {
        $setting = Setting::pluck('value','key')->toArray();
        $frontSetting = \App\Models\FrontSetting::where('page','home')->pluck('value','key')->toArray();

        return view('theme.contact_us',compact('setting','frontSetting'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function client()
    {
        $clients =  Client::all()->sortByDesc('updated_at');

        return view('theme.client',compact('clients'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gallery()
    {
        $galleries = Gallery::all()->sortByDesc('updated_at');

        return view('theme.gallery',compact('galleries'));
    }
}
