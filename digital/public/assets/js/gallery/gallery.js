/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/gallery/gallery.js":
/*!************************************************!*\
  !*** ./resources/assets/js/gallery/gallery.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var tableName = '#galleryTbl';
$(tableName).DataTable({
  processing: true,
  serverSide: true,
  'order': [[0, 'asc']],
  ajax: {
    url: galleryUrl
  },
  columnDefs: [{
    'targets': [0],
    'orderable': false
  }, {
    'targets': [1],
    'orderable': false,
    'className': 'text-center',
    'width': '165px'
  }],
  columns: [{
    data: function data(row) {
      return '<img src="' + row.image_url + '" class="rounded-circle thumbnail-rounded img-bg"' + '</img>';
    },
    name: 'id'
  }, {
    data: function data(row) {
      return "<a title=\"Show\" class=\"btn btn-secondary action-btn show-btn\" data-id=\"".concat(row.id, "\" href=\"#\">\n                            <i class=\"fa fa-eye\"></i>\n                        </a>\n                      <a title=\"Edit\" class=\"btn btn-warning action-btn edit-btn\" id=\"").concat(row.id, "\" onclick=\"renderData(").concat(row.id, ")\" data-toggle=\"modal\"  data-keyboard=\"true\"><i class=\"fa fa-edit\"></i></a>") + "<a title=\"Delete\" class=\"btn btn-danger action-btn delete-btn\" id=\"".concat(row.id, "\" onclick=\"deleteData(").concat(row.id, ")\"><i class=\"fa fa-trash\"></i></a>");
    },
    name: 'id'
  }]
});
$('.addGalleryModal').click(function () {
  $('#addModal').appendTo('body').modal('show');
});
$(document).on('submit', '#createForm', function (e) {
  e.preventDefault();
  processingBtn('#createForm', '#btnSave', 'loading');
  $.ajax({
    url: gallerySaveUrl,
    type: 'POST',
    data: new FormData($(this)[0]),
    processData: false,
    contentType: false,
    success: function success(result) {
      if (result.success) {
        displaySuccessMessage(result.message);
        $('#addModal').modal('hide');
        $('#galleryTbl').DataTable().ajax.reload(null, false);
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    },
    complete: function complete() {
      processingBtn('#createForm', '#btnSave');
    }
  });
});

window.renderData = function (id) {
  $.ajax({
    url: galleryUrl + '/' + id + '/edit',
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        $('#galleryId').val(result.data.id);
        $('#galleryEditImagePreview').attr('src', result.data.image_url);
        $('#imageUrl').attr('href', result.data.image_url);
        $('#imageUrl').text('view');
        $('#editModal').appendTo('body').modal('show');
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    }
  });
};

$(document).on('submit', '#editForm', function (event) {
  event.preventDefault();
  processingBtn('#editForm', '#btnEditSave', 'loading');
  var id = $('#galleryId').val();
  $.ajax({
    url: galleryUrl + '/' + id + '/update',
    type: 'POST',
    data: new FormData($(this)[0]),
    processData: false,
    contentType: false,
    success: function success(result) {
      if (result.success) {
        displaySuccessMessage(result.message);
        $('#editModal').modal('hide');
        $('#galleryTbl').DataTable().ajax.reload(null, false);
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    },
    complete: function complete() {
      processingBtn('#editForm', '#btnEditSave');
    }
  });
});
$(document).on('click', '.show-btn', function (event) {
  var galleryId = $(event.currentTarget).attr('data-id');
  $.ajax({
    url: galleryUrl + '/' + galleryId,
    type: 'GET',
    success: function success(result) {
      if (result.success) {
        $('#show_image').html('');
        $('#show_created_at').html('');
        $('#show_updated_at').html('');
        $('#show_image').attr('src', result.data.image_url);
        var createdDate = moment(result.data.created_at, 'YYYY-MM-DD hh:mm:ss').format('Do MMM, YYYY hh:mm A');
        $('#show_created_at').append(createdDate);
        var updateDate = moment(result.data.updated_at, 'YYYY-MM-DD hh:mm:ss').format('Do MMM, YYYY hh:mm A');
        $('#show_updated_at').append(updateDate);
        $('#showModal').appendTo('body').modal('show');
      }
    },
    error: function error(result) {
      displayErrorMessage(result.responseJSON.message);
    }
  });
});

window.deleteData = function (id) {
  deleteItem(galleryUrl + '/' + id, '#galleryTbl', 'Gallery');
};

var defaultImage = $('#galleryImagePreview').attr('src');
$('#addModal').on('hidden.bs.modal', function () {
  $('#galleryImagePreview,#galleryEditImagePreview').attr('src', defaultImage);
  resetModalForm('#createForm', '#validationErrorsBox');
});
$('#editModal').on('hidden.bs.modal', function () {
  $('#galleryImagePreview,#galleryEditImagePreview').attr('src', defaultImage);
  resetModalForm('#editForm', '#editValidationErrorsBox');
});
$(document).on('change', '#galleryImage', function () {
  var validFile = isValidFile($(this), '#validationErrorsBox');

  if (validFile) {
    displayPhoto(this, '#galleryImagePreview');
  } else {
    $(this).val('');
  }
});
$(document).on('change', '#galleryEditImage', function () {
  var validFile = isValidFile($(this), '#editValidationErrorsBox');

  if (validFile) {
    displayPhoto(this, '#galleryEditImagePreview');
  } else {
    $(this).val('');
  }
});

/***/ }),

/***/ 9:
/*!******************************************************!*\
  !*** multi ./resources/assets/js/gallery/gallery.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\wamp64\www\Work\jkpubliclycity\digital\resources\assets\js\gallery\gallery.js */"./resources/assets/js/gallery/gallery.js");


/***/ })

/******/ });