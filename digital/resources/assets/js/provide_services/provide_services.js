'use strict';

let tableName = '#provideServicesTbl';
$(tableName).DataTable({
    processing: true,
    serverSide: true,
    'order': [[1, 'asc']],
    ajax: {
        url: provideServiceUrl,
    },
    columnDefs: [
        {
            'targets': [0],
            'orderable': false,
        },
        {
            'targets': [1],
            'orderable': false,
            'width' : '50%',
        },
        {
            'targets': [2],
            'orderable': false,
            'className': 'text-center',
            'width': '165px',
        },
    ],
    columns: [
        {
            data: function (row) {
                return '<img src="' + row.image +
                    '" class="rounded-circle thumbnail-rounded img-bg"' +
                    '</img>';
            },
            name: 'id',
        },
        {
            data: function (row) {
                return row.name;
            },
            name: 'id',
        },
        {
            data: function (row) {
                return `<a title="Show" class="btn btn-secondary action-btn show-btn" data-id="${row.id}" href="#">
                            <i class="fa fa-eye"></i>
                        </a>
                      <a title="Edit" class="btn btn-warning action-btn edit-btn" id="${row.id}" onclick="renderData(${row.id})" data-toggle="modal"  data-keyboard="true"><i class="fa fa-edit"></i></a>` +
                    `<a title="Delete" class="btn btn-danger action-btn delete-btn" id="${row.id}" onclick="deleteData(${row.id})"><i class="fa fa-trash"></i></a>`;
            }, name: 'id',
        },
    ],
});

$('.addProvideServicesModal').click(function () {
    $('#addModal').appendTo('body').modal('show');
});

$(document).on('submit', '#createForm', function (e) {
    e.preventDefault();
    processingBtn('#createForm', '#btnSave', 'loading');
    $.ajax({
        url: provideServiceSaveUrl,
        type: 'POST',
        data: new FormData($(this)[0]),
        processData: false,
        contentType: false,
        success: function (result) {
            if (result.success) {
                displaySuccessMessage(result.message);
                $('#addModal').modal('hide');
                $('#provideServicesTbl').DataTable().ajax.reload(null, false);
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
        complete: function () {
            processingBtn('#createForm', '#btnSave');
        },
    });
});

window.renderData = function (id) {
    $.ajax({
        url: provideServiceUrl + '/' + id + '/edit',
        type: 'GET',
        success: function (result) {
            if (result.success) {
                $('#provideServicesId').val(result.data.id);
                $('#editName').val(result.data.name);
                $('#editImagePreview').attr('src', result.data.image);
                $('#imageUrl').
                attr('href', result.data.image);
                $('#imageUrl').text('view');
                $('#editModal').appendTo('body').modal('show');
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
    });
};

$(document).on('submit', '#editForm', function (event) {
    event.preventDefault();
    processingBtn('#editForm', '#btnEditSave', 'loading');
    const id = $('#provideServicesId').val();
    $.ajax({
        url: provideServiceUrl + '/' + id + '/update',
        type: 'POST',
        data: new FormData($(this)[0]),
        processData: false,
        contentType: false,
        success: function (result) {
            if (result.success) {
                displaySuccessMessage(result.message);
                $('#editModal').modal('hide');
                $('#provideServicesTbl').DataTable().ajax.reload(null, false);
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
        complete: function () {
            processingBtn('#editForm', '#btnEditSave');
        },
    });
});

$(document).on('click', '.show-btn', function (event) {
    let provideServiceId = $(event.currentTarget).attr('data-id');
    $.ajax({
        url: provideServiceUrl + '/' + provideServiceId,
        type: 'GET',
        success: function (result) {
            if (result.success) {
                $('#show_created_at').html('');
                $('#show_updated_at').html('');
                $('#show_name').html('');
                $('#show_image').html('');
                $('#show_name').append(result.data.name);
                $('#show_image').
                attr('src', result.data.image);
                let createdDate = moment(result.data.created_at,
                    'YYYY-MM-DD hh:mm:ss').
                format('Do MMM, YYYY hh:mm A');
                $('#show_created_at').append(createdDate);
                let updateDate = moment(result.data.updated_at,
                    'YYYY-MM-DD hh:mm:ss').
                format('Do MMM, YYYY hh:mm A');
                $('#show_updated_at').append(updateDate);
                $('#showModal').appendTo('body').modal('show');
            }
        },
        error: function (result) {
            displayErrorMessage(result.responseJSON.message);
        },
    });
});

window.deleteData = function (id) {
    deleteItem(provideServiceUrl + '/' + id, '#provideServicesTbl', 'Provide Service');
};

let defaultImage = $('#imagePreview').attr('src');

$('#addModal').on('hidden.bs.modal', function () {
    $('#imagePreview,#editImagePreview').attr('src', defaultImage);
    resetModalForm('#createForm', '#validationErrorsBox');
});

$('#editModal').on('hidden.bs.modal', function () {
    $('#imagePreview,#editImagePreview').attr('src', defaultImage);
    resetModalForm('#editForm', '#editValidationErrorsBox');
});

$(document).on('change', '#image', function () {
    let validFile = isValidFile($(this), '#validationErrorsBox');
    if (validFile) {
        displayPhoto(this, '#imagePreview');
    } else {
        $(this).val('');
    }
});

$(document).on('change', '#editImage', function () {
    let validFile = isValidFile($(this),
        '#editValidationErrorsBox');
    if (validFile) {
        displayPhoto(this, '#editImagePreview');
    } else {
        $(this).val('');
    }
});
