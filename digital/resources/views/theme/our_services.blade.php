@extends('theme.layout.app')
@section('title')
    Services
@endsection
@push('css')
    <link rel="stylesheet" href="{{ mix('assets/css/web/our_services.css')}}">
@endpush
@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container animate__animated animate__bounce">

            <ol>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Services</li>
            </ol>
            <h2>Services</h2>

        </div>
    </section>
    <!-- End Breadcrumbs -->

{{--    <section id="heroServices">--}}
{{--        <div class="container">--}}
{{--            <div class="owl-carousel services-carousel">--}}
{{--                @foreach($provideServices as $provideService)--}}
{{--                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">--}}
{{--                        <div class="carousel-container">--}}
{{--                            <div class="carousel-content">--}}
{{--                                <img src="{{ $provideService->image }}">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Clients Section !-->

    <!-- ======= Services Section ======= -->

    <section id="servicesSection" class="servicesSection">
        <div class="container">
            <div class="row">
                @foreach($provideServices as $provideService)
                    <div data-aos="{{ $loop->odd ? 'fade-up-left' : 'fade-up-right' }}" 
                         class="col-lg-4" style="margin-bottom:180px; ">
                        <div class="icon-box ">
                            <img class="hvr-wobble-horizontal" alt="" src="{{ $provideService->image }}" style="border-radius: 10px;width: 100%;height: 100%;">
                            <div class="section-title mt-2" style="padding-bottom: 0" data-aos="fade-up"  data-aos-delay="500">
                                <h2 style="text-overflow: ellipsis;  white-space: nowrap;
  overflow: hidden;">{{ $provideService->name }}</h2>
                            </div>
                            <div class="row justify-content-center ">
                                <div class="justify-content-center  hvr-float-shadow d-flex our-provide-services mb-3" data-aos="fade-up">
                                    <a class="our-provide-services-btn ">Inquiry Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

{{--    <section id="services" class="services-section">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                @foreach($provideServices as $provideService)--}}
{{--                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-2">--}}
{{--                        <div class="icon-box">--}}
{{--                            <img src="{{ $provideService->image }}" style="width: 50%;">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="section-title" data-aos="fade-up">--}}
{{--                        <h2>{{ $provideService->name }}</h2>--}}
{{--                    </div>--}}
{{--                    <div class="justify-content-center d-flex mt-4 home-provide-services" data-aos="fade-up">--}}
{{--                        <h2 class="home-provide-services-btn">Inquiry Now</h2>--}}
{{--                        <p>Inquiry Now</p>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Services Section -->

    <!-- ======= Our Skills Section ======= -->
{{--    <section id="skills" class="skills">--}}
{{--        <div class="container">--}}

{{--            <div class="section-title">--}}
{{--                <h2>Our Skills</h2>--}}
{{--                <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>--}}
{{--            </div>--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <img src="assets/img/skills-img.jpg" class="img-fluid" alt="">--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 pt-4 pt-lg-0 content">--}}
{{--                    <h3>Voluptatem dignissimos provident quasi corporis voluptates</h3>--}}
{{--                    <p class="font-italic">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt direna past reda--}}
{{--                    </p>--}}

{{--                    <div class="skills-content">--}}

{{--                        <div class="progress">--}}
{{--                            <span class="skill">HTML <i class="val">100%</i></span>--}}
{{--                            <div class="progress-bar-wrap">--}}
{{--                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="progress">--}}
{{--                            <span class="skill">CSS <i class="val">90%</i></span>--}}
{{--                            <div class="progress-bar-wrap">--}}
{{--                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="progress">--}}
{{--                            <span class="skill">JavaScript <i class="val">75%</i></span>--}}
{{--                            <div class="progress-bar-wrap">--}}
{{--                                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="progress">--}}
{{--                            <span class="skill">Photoshop <i class="val">55%</i></span>--}}
{{--                            <div class="progress-bar-wrap">--}}
{{--                                <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </section>--}}
    <!-- End Our Skills Section -->

</main><!-- End #main -->
@endsection
@push('scripts')
    <script>
    $('.services-carousel').owlCarousel({
        margin: 10,
        autoplay: true,
        loop: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsiveClass: false,
        dots: true,
        responsive: {
            0: {
                items: 1,
            },
            320: {
                items: 1,
                margin: 20,
            },
            540: {
                items: 1,
            },
            600: {
                items: 1,
            },
        },
    });
    </script>
@endpush
