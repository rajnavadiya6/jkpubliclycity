@extends('theme.layout.app')
@section('title')
    Contact Us
@endsection

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container animate__animated animate__bounce">

            <ol>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Contact</li>
            </ol>
            <h2>Contact</h2>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="row">
                <div class="col-lg-6 hvr-wobble-horizontal">
                    <div class="info-box mb-4"  data-aos="flip-left"
                         data-aos-duration="1000">
                        <i class="bx bx-map"></i>
                        <h3>Our Address</h3>
                        <p>{!! $setting['address'] !!}</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 hvr-wobble-horizontal">
                    <div class="info-box  mb-4" data-aos="flip-up"  data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <i class="bx bx-envelope"></i>
                        <h3>Email Us</h3>
                        <p>{{ $setting['email'] }}</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 hvr-wobble-horizontal">
                    <div class="info-box  mb-4" data-aos="flip-right" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <i class="bx bx-phone-call"></i>
                        <h3>Call Us</h3>
                        <p>{{ $setting['phone']}}</p>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-6 hvr-wobble-horizontal">
                    <iframe src="{{ $frontSetting['map_location'] }}" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
                </div>

                <div class="col-lg-6" data-aos-duration="1000" data-aos="flip-up">
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session('success') }}
                        </div>
                    @endif
                    @include('flash::message')
                    <form action="{{ route('contact-us.store') }}" method="post" role="form" class="contact-us-form">
                        @csrf
                        <div class="row" >
                            <div class="col-md-6 form-group">
                                <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" id="name" autofocus placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                @if ($errors->has('name'))
                                    <span class="invalid feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}.</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group mt-3 mt-md-0">
                                <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                @if ($errors->has('email'))
                                    <span class="invalid feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}.</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ old('subject') }}" autofocus id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                            @if ($errors->has('subject'))
                                <span class="invalid feedback" role="alert">
                                    <strong>{{ $errors->first('subject') }}.</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" rows="5"  autofocus data-rule="required" data-msg="Please write something for us" placeholder="Message">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                                <span class="invalid feedback" role="alert">
                                    <strong>{{ $errors->first('message') }}.</strong>
                                </span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <div class="loading">Loading</div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center">
                            <button type="submit">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Section -->

</main><!-- End #main -->
@endsection
