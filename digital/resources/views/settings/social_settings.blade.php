@extends('settings.index')
@section('title')
    Social Settings
@endsection
@section('section')
    {{ Form::open(['route' => 'settings.update','id'=>'editForm']) }}
    <div class="row mt-3">
        <div class="form-group col-sm-6">
            {{ Form::label('facebook_url', 'Facebook Url'.':') }}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fab fa-facebook-f facebook-fa-icon"></i>
                    </div>
                </div>
                {{ Form::text('facebook_url', $setting['facebook_url'], ['class' => 'form-control','id'=>'facebookUrl']) }}
            </div>
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('twitter_url', 'Twitter Url'.':') }}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fab fa-twitter twitter-fa-icon"></i>
                    </div>
                </div>
                {{ Form::text('twitter_url', $setting['twitter_url'], ['class' => 'form-control','id'=>'twitterUrl']) }}
            </div>
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('instagram_url', 'Instagram Url'.':') }}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fab fa-instagram"></i>
                    </div>
                </div>
                {{ Form::text('instagram_url', $setting['instagram_url'], ['class' => 'form-control','id'=>'googlePlusUrl']) }}
            </div>
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('pinterest_url', 'Pinterest Url'.':') }}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fab fa-pinterest"></i>
                    </div>
                </div>
                {{ Form::text('pinterest_url', $setting['pinterest_url'], ['class' => 'form-control','id'=>'googlePlusUrl']) }}
            </div>
        </div>
        <div class="form-group col-6 col-sm-12">
            {{ Form::label('youtube_url','YouTube Url'.':') }}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fab fa-youtube"></i>
                    </div>
                </div>
                {{ Form::text('youtube_url', $setting['youtube_url'], ['class' => 'form-control','id'=>'linkedInUrl']) }}
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {{ Form::submit('Save', ['class' => 'btn btn-primary','id'=>'submitId']) }}
            {{ Form::reset('Cancel', ['class' => 'btn btn-secondary text-dark']) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection
