@extends('layouts.app')
@section('title')
    Provide Services
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Provide Services</h1>
            <div class="section-header-breadcrumb">
                <a class="btn btn-primary addProvideServicesModal">Provide Services
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('provide_services.table')
                </div>
            </div>
        </div>
    </section>
    @include('provide_services.add_modal')
    @include('provide_services.edit_modal')
    @include('provide_services.show')
@endsection
@push('scripts')
    <script>
        let provideServiceUrl = "{{ route('provide-services.index') }}";
        let provideServiceSaveUrl = "{{ route('provide-services.store') }}";
    </script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{mix('assets/js/provide_services/provide_services.js')}}"></script>
@endpush
