@extends('layouts.app')
@section('title')
    Gallery
@endsection
@push('css')
    <link href="{{ asset('assets/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Gallery</h1>
            <div class="section-header-breadcrumb">
                <a class="btn btn-primary addGalleryModal">Gallery
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
           <div class="card">
                <div class="card-body">
                    @include('gallery.table')
                </div>
           </div>
       </div>
    </section>
    @include('gallery.add_modal')
    @include('gallery.edit_modal')
    @include('gallery.show')
@endsection
@push('scripts')
    <script>
        let galleryUrl = "{{ route('gallery.index') }}";
        let gallerySaveUrl = "{{ route('gallery.store') }}";
    </script>
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{mix('assets/js/gallery/gallery.js')}}"></script>
@endpush
